package za.co.ab.page;

import io.restassured.path.json.JsonPath;
import za.co.ab.config.EndPoint;
import za.co.ab.config.ReuseableSpecifications;
import net.serenitybdd.rest.SerenityRest;
import io.restassured.response.Response;
import io.restassured.http.ContentType;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

public class DogCEOTests extends ReuseableSpecifications {

    public void extractAllBreeds() {
        SerenityRest.rest()
           .given().
                spec(getGenericRequestSpec())
           .when().
                get(EndPoint.getAllBreedList)
            .then().
                contentType(ContentType.JSON).
                log().all();
    }

    public void verifyThatRetrieverIsWithInList(String value) {
        String response = SerenityRest.rest()
           .given().
                spec(getGenericRequestSpec())
           .when().
                get(EndPoint.getAllBreedList).jsonPath().getString(EndPoint.verifyRetrieverWithInList);

        if (response.contains(value)) {
            System.out.println("Verify - retriever is within list");
        } else {
            System.out.println("retriever not within list");
        }
    }

    public void extractSubRetrieverBreed() {
        Response listOfBreed = SerenityRest.rest()
           .given().
                spec(getGenericRequestSpec())
           .when().
                get(EndPoint.getAllBreedList)
           .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().response();

        List<String> subBreed = listOfBreed.path(EndPoint.getGetRetrieverWithInList);

        for (String breedName : subBreed) {
            System.out.println(breedName);
            //assertThat(breedName);
        }
    }

    public void extractRandomImage() {
        SerenityRest.rest().given().
                spec(getGenericRequestSpec()).
                get(EndPoint.getBreedRandomImage).then().log().all();
    }

}